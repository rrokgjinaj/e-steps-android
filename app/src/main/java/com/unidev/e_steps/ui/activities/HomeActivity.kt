package com.unidev.e_steps.ui.activities

import android.bluetooth.BluetoothDevice
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import com.unidev.e_steps.R
import com.unidev.e_steps.ui.fragments.scan.ScanDevicesFragment
import com.unidev.e_steps.ui.fragments.hometabs.HomeTabsAdapter

class HomeActivity : NavigationDrawerActivity(),
    ScanDevicesFragment.Handlers {

    override fun onConnection(device: BluetoothDevice) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_home)

        initToolbar(findViewById(R.id.toolbar), findViewById(
            R.id.drawer_layout
        ))

        val sectionsPagerAdapter =
            HomeTabsAdapter(
                this,
                supportFragmentManager
            )

        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)

    }


}