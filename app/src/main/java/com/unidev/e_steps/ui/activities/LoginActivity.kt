package com.unidev.e_steps.ui.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.unidev.e_steps.*
import com.unidev.e_steps.ui.utils.PreferencesManager

import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val login = findViewById<Button>(R.id.login)

        val savedUser = PreferencesManager.getUser(this)
        if (savedUser.first != null && savedUser.second != null) {
            username.setText(savedUser.first)
            password.setText(savedUser.second)
            login.callOnClick()
        }
    }
    private fun updateUiWithUser() {
        val welcome = getString(R.string.welcome)
        Toast.makeText(
            applicationContext,
            "$welcome ${username.text}",
            Toast.LENGTH_LONG
        ).show()

        PreferencesManager.setUser(this, username.text.toString(), password.text.toString())
        if(PreferencesManager.getSavedDevices(this).isEmpty())
            startActivity(Intent(this, StartConfigurationActivity::class.java))
        else{
            startActivity(Intent(this, HomeActivity::class.java))
        }
    }

    fun goRegister(view: View) {
        startActivity(Intent(this, RegisterActivity::class.java))
    }

    fun login(view: View) {

        /*Volley.newRequestQueue(this).add(
        JsonObjectRequest(Request.Method.POST,"http://192.168.43.216:9000/login/estepper/",null,Response.Listener{
            response ->
            Toast.makeText(
                applicationContext,
                response.toString(),
                Toast.LENGTH_LONG
            ).show()

        },Response.ErrorListener { error ->
            Toast.makeText(
                applicationContext,
                error.toString(),
                Toast.LENGTH_LONG
            ).show()
        })
        )*/
        updateUiWithUser()

    }
}