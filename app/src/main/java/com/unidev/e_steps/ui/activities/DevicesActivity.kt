package com.unidev.e_steps.ui.activities

import android.bluetooth.BluetoothDevice
import android.os.Bundle
import com.unidev.e_steps.R
import com.unidev.e_steps.ui.fragments.scan.ScanDevicesFragment

class DevicesActivity : NavigationDrawerActivity(),
    ScanDevicesFragment.Handlers {
    override fun onConnection(device: BluetoothDevice) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_devices)

        initToolbar(findViewById(R.id.toolbar), findViewById(
            R.id.drawer_layout
        ))
    }
}
