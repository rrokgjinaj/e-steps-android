package com.unidev.e_steps.ui.activities

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import androidx.core.view.children
import com.unidev.e_steps.R

class ProfileActivity : NavigationDrawerActivity() {

    private lateinit var root: LinearLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        root = findViewById<LinearLayout>(R.id.profile)
        initToolbar(findViewById(R.id.toolbar), findViewById(R.id.drawer_layout))
    }

    fun modify(view: View) {

        root.children.filter { it is EditText }.forEach {
            it.isEnabled=true
            (it as EditText).setTextColor(getColor(R.color.black))
        }
        root.findViewById<Button>(R.id.save_profile).setBackgroundResource(R.drawable.button_background)

    }

    fun saveProfile(view: View) {

        root.children.filter { it is EditText }.forEach {
            it.isEnabled=false
            (it as EditText).setTextColor(getColor(android.R.color.darker_gray))
        }
        root.findViewById<Button>(R.id.save_profile).setBackgroundResource(R.drawable.button_unselected)
    }
}
