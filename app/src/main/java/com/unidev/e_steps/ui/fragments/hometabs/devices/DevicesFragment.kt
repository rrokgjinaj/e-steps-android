package com.unidev.e_steps.ui.fragments.hometabs.devices
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.fragment.app.DialogFragment
import com.unidev.e_steps.ui.utils.PreferencesManager
import com.unidev.e_steps.R
import com.unidev.e_steps.ui.fragments.scan.ScanDevicesFragment
import com.unidev.e_steps.data.CustomBTState
import com.unidev.e_steps.data.CustomDevice
import com.unidev.e_steps.ui.utils.BLEUtils


/**
 * A fragment representing a list of Items.
 */
class DevicesFragment : Fragment() {

    lateinit var ble: BLEUtils
    var devices: Set<CustomDevice> = emptySet()
    var recyclerView: RecyclerView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        ble = BLEUtils(activity!!)
        val view = inflater.inflate(R.layout.fragment_devices_list, container, false)
        val addDeviceButton = view.findViewById<FloatingActionButton>(R.id.addDevice)
        recyclerView = view.findViewById(R.id.devices_list)

        addDeviceButton.setOnClickListener {
            val f = ScanDevicesFragment()
            f.setStyle(DialogFragment.STYLE_NORMAL,
                R.style.dialog
            )
            f.show(fragmentManager, "scan")
        }

        with(recyclerView!!) {
            layoutManager = LinearLayoutManager(context)
            devices = PreferencesManager.getSavedDevices(
                activity!!
            ).map { CustomDevice(it.key, it.value, CustomBTState.Unavailable) }.toSet()
            adapter =
                ConnectedDeviceAdapter(
                    context,
                    devices
                )
        }
        return view
    }

    private val callback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)
            devices.firstOrNull { it.address == result?.device?.address }?.state = CustomBTState.Online
            //Toast.makeText(context, devices.first().state.toString(),Toast.LENGTH_SHORT).show()
            recyclerView!!.adapter!!.notifyDataSetChanged()
        }
    }

    override fun onResume() {
        super.onResume()
        recyclerView!!.adapter!!.notifyDataSetChanged()
        ble.findNearbyDevices(callback)
    }

    override fun onPause() {
        super.onPause()
        ble.stop(callback)
    }
}
