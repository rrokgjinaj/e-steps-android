package com.unidev.e_steps.ui.fragments.hometabs.home

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.charts.LineChart
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.unidev.e_steps.R
import com.unidev.e_steps.ui.activities.RecordActivity
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineDataSet
import android.graphics.DashPathEffect
import android.widget.Button
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.formatter.ValueFormatter


/**
 * A placeholder fragment containing a simple view.
 */
class HomeFragment : Fragment() {
    lateinit var chart: LineChart

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val root = inflater.inflate(R.layout.fragment_home, container, false)

        chart= root.findViewById(R.id.chart1)

        // background color
        chart.setBackgroundColor(Color.WHITE)

        // disable description text
        chart.description.isEnabled = false

        // enable touch gestures
        chart.setTouchEnabled(true)

        // enable scaling and dragging
        chart.isDragEnabled = true
        chart.setScaleEnabled(true)
        // force pinch zoom along both axis
        chart.setPinchZoom(true)
        chart.legend.isEnabled = false

        val xAxis: XAxis
        run {
            // // X-Axis Style // //
            xAxis = chart.getXAxis()
        }

        val yAxis: YAxis
        run {
            // // Y-Axis Style // //
            yAxis = chart.getAxisLeft()

            // disable dual axis (only use LEFT axis)
            chart.getAxisRight().setEnabled(false)

            // horizontal grid lines
            yAxis.enableGridDashedLine(10f, 10f, 0f)

            yAxis.valueFormatter= object : ValueFormatter(){
                override fun getFormattedValue(value: Float): String {
                    return when(value){
                        0F-> "High Risk"
                        1F-> "Medium Risk"
                        else -> "Low Risk"
                    }
                }
            }
            yAxis.granularity= 1F
        }
        setWeekData()

        root.findViewById<FloatingActionButton>(R.id.goRecord).setOnClickListener {
            startActivity( Intent(activity, RecordActivity::class.java))
        }


        val weekButton = root.findViewById<Button>(R.id.weekGraph)
        val monthButton = root.findViewById<Button>(R.id.monthGraph)

        weekButton.setOnClickListener {
            setWeekData()
            weekButton.setBackgroundResource(R.drawable.button_background)
            monthButton.setBackgroundResource(R.drawable.button_unselected)
        }
        monthButton.setOnClickListener {
            setMonthData()
            monthButton.setBackgroundResource(R.drawable.button_background)
            weekButton.setBackgroundResource(R.drawable.button_unselected)
         }


        return root
    }
            private val values= listOf(
                Entry(0f, 0f),
                Entry(1f, 1f),
                Entry(2f, 1f),
                Entry(3f, 2f),
                Entry(4f, 1f),
                Entry(5f, 2f),
                Entry(6f, 2f),
                Entry(7f, 0f),
                Entry(8f, 1f),
                Entry(9f, 1f),
                Entry(10f, 2f),
                Entry(11f, 1f),
                Entry(12f, 2f),
                Entry(13f, 2f),
                Entry(14f, 0f),
                Entry(15f, 1f),
                Entry(16f, 1f),
                Entry(17f, 2f),
                Entry(18f, 1f),
                Entry(19f, 2f),
                Entry(20f, 2f),
                Entry(21f, 0f),
                Entry(22f, 1f),
                Entry(23f, 1f),
                Entry(24f, 2f),
                Entry(25f, 1f),
                Entry(26f, 2f),
                Entry(27f, 2f)

            )
    private fun setWeekData() {

        val set = LineDataSet(values.takeLast(7),"Helth Line")
        with(set) {
            enableDashedLine(10f, 5f, 0f);
            formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
            enableDashedHighlightLine(10f, 5f, 0f)

            color = Color.BLACK
            setCircleColor(Color.BLACK)

            lineWidth = 1f
            formLineWidth = 1f
            formSize = 15f

            setDrawIcons(false)
            setDrawCircles(true)
            setDrawCircleHole(false)
            setDrawValues(false)
            setDrawFilled(true)

            chart.data = LineData(listOf(this))
            chart.invalidate()
        }


    }

    private fun setMonthData() {
        val set = LineDataSet(values,"Helth Line")
        with(set) {
            enableDashedLine(10f, 5f, 0f);
            formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
            enableDashedHighlightLine(10f, 5f, 0f)

            color = Color.BLACK
            setCircleColor(Color.BLACK)

            lineWidth = 1f
            formLineWidth = 1f
            formSize = 15f

            setDrawIcons(false)
            setDrawCircles(true)
            setDrawCircleHole(false)
            setDrawValues(false)
            setDrawFilled(true)
            chart.data = LineData(listOf(this))
            chart.invalidate()
        }


    }
}