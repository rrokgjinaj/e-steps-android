package com.unidev.e_steps.ui.fragments.scan

import android.bluetooth.BluetoothDevice
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.unidev.e_steps.R
import com.unidev.e_steps.ui.utils.BLEUtils
import com.unidev.e_steps.ui.utils.PreferencesManager
import java.util.*

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [ScanDevicesFragment.Handlers] interface.
 */
class ScanDevicesFragment(): DialogFragment() {
    private enum class PermissionCodes {LOCATION, BLUETOOTH, GPS}

    private lateinit var ble: BLEUtils

    //private var listener: Handlers? = null
    var devices: SortedSet<BluetoothDevice> = sortedSetOf(compareBy{it.address})

    private var recyclerView: RecyclerView? = null

    private var listener: Handlers? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_scan, container, false)
        // Set the adapter
        recyclerView = view.findViewById(R.id.DeviceList)
        with(recyclerView!!) {
            layoutManager = LinearLayoutManager(context)
            adapter = ScanDeviceAdapter(
                context,
                devices
            )
        }

        dialog.setTitle("Connect to your insole")

        return view
    }

    override fun onStart() {
        super.onStart()
        startScan()
    }

    override fun onStop() {
        super.onStop()
        ble.stop(scanCallback)
        ble.disconnectAll()

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        ble = BLEUtils(this)
        if (context !is Handlers)
            throw RuntimeException("$context must implement Handlers")
        else
            listener = context

    }

    interface Handlers {
        fun onConnection(device: BluetoothDevice)
    }

    private fun startScan() {
        devices.clear()
        ble.findNearbyDevices(scanCallback)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PermissionCodes.LOCATION.ordinal ->
                if (grantResults.all { it == PackageManager.PERMISSION_GRANTED })
                    startScan()
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private val scanCallback = object : ScanCallback() {
        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)
            Toast.makeText(context, "Scan failed $errorCode", Toast.LENGTH_LONG).show()
        }

        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)
            if(result?.device?.name?.contains("Insole") == true &&
                !PreferencesManager.getSavedDevices(activity!!).containsKey(result.device.address)) {
                devices.add(result.device)
                recyclerView!!.adapter!!.notifyDataSetChanged()
            }
        }
    }
}
