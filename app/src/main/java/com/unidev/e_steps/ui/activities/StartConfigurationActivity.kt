package com.unidev.e_steps.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.unidev.e_steps.R

class StartConfigurationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start_configuration)
    }

    fun firstConfiguration(view: View) {
        startActivity( Intent(this, ScanSettingsActivity::class.java))
    }

}
