package com.unidev.e_steps.ui.fragments.hometabs.devices

import android.content.Context
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.unidev.e_steps.R
import com.unidev.e_steps.data.CustomDevice
import com.unidev.e_steps.ui.fragments.AbstractDeviceAdapter

class ConnectedDeviceAdapter(val context: Context, private val data: Set<CustomDevice>) :
    AbstractDeviceAdapter<CustomDevice, ConnectedDeviceAdapter.ViewHolder>(context, data,
        R.layout.connected_device_row, {
            ViewHolder(
                it
            )
        }) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.name.text = data.elementAt(position).name
        holder.status.text= data.elementAt(position).state.toString()
    }

    class ViewHolder(itemView: View) : AbstractViewHolder<CustomDevice>(itemView) {
        val name: TextView = itemView.findViewById(R.id.deviceName)
        val status: TextView = itemView.findViewById(R.id.status)
        override fun onItemClick(device: CustomDevice) {
            Toast.makeText(itemView.context, name.text, Toast.LENGTH_LONG).show()
        }
    }
}