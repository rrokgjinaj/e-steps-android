package com.unidev.e_steps.ui.utils

import android.app.Activity
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.graphics.Point

object PreferencesManager {
    private const val PREFERENCEFILE = "Preferences"
    private const val SAVEDDEVICES = "SavedDevices"
    private const val USERMAIL = "UserMail"
    private const val USERPASSWORD = "UserPassword"
    private const val COPS = "Cops"
    private const val MAXP = "MaxsPressure"

    private fun preferences(activity: Activity) = activity.getSharedPreferences(PREFERENCEFILE, Context.MODE_PRIVATE)

    fun setUser(activity: Activity, email: String, password: String) {
        val preference =
            preferences(activity)
        preference.edit().putString(USERMAIL, email).putString(
            USERPASSWORD,password).apply()
    }
    fun getUser(activity: Activity): Pair<String?, String?> {
        val preference =
            preferences(activity)
        return Pair(preference.getString(USERMAIL, null), preference.getString(
            USERPASSWORD, null))
    }
    fun removeUser(activity: Activity) {
        val preference =
            preferences(activity)
        preference.edit().remove(USERMAIL).remove(
            USERPASSWORD
        ).apply()
    }

    fun addDevice(activity: Activity, device: BluetoothDevice) {
        val saved = getSavedDevices(
            activity
        ) + Pair(device.address,device.name)
        preferences(activity).edit().putStringSet(
            SAVEDDEVICES, saved.entries.map { it.key+ "," + it.value }.toSet()).apply()

    }
    fun removeDevice(activity: Activity, device: BluetoothDevice)
    {
        val saved =
            getSavedDevices(
                activity
            )
        if (saved.containsKey(device.address))

            preferences(activity).edit().putStringSet(
                SAVEDDEVICES,  (saved - device.address).entries.map { it.key+ "," + it.value }.toSet()).apply()
    }
    fun getSavedDevices(activity: Activity): Map<String,String> {

        val preference =
            preferences(activity)
        return preference.getStringSet(SAVEDDEVICES, setOf())!!.map { it.split(",") }.map { it[0] to it[1] }.toMap()
    }

    fun savePressures(activity: Activity, cops: MutableList<Point>, maxPressures: MutableList<Double>) {
        val preference =
            preferences(activity)
        preference.edit().putString(COPS, cops.joinToString()).apply()
        preference.edit().putString(MAXP, maxPressures.joinToString()).apply()
    }
}