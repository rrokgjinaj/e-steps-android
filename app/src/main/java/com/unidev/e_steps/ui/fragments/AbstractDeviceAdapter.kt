package com.unidev.e_steps.ui.fragments

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class AbstractDeviceAdapter<T, VH : AbstractDeviceAdapter.AbstractViewHolder<T>>(context: Context,
                                                                                                                                             private val data: Set<T>,
                                                                                                                                             private val rowLayout: Int,
                                                                                                                                             private val createViewHolder : (View) -> VH) :
    RecyclerView.Adapter<VH>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val view = inflater.inflate(rowLayout, parent,false)
        return createViewHolder(view)
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.itemView.setOnClickListener { holder.onItemClick(data.elementAt(position)) }
    }

    override fun getItemCount(): Int = data.size

    abstract class AbstractViewHolder<I>(itemView: View) : RecyclerView.ViewHolder(itemView) {
        abstract fun onItemClick(device: I)
    }
}