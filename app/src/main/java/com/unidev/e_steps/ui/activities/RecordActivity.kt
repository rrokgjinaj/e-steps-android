package com.unidev.e_steps.ui.activities

import android.bluetooth.*
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.content.Context
import android.graphics.Color
import android.graphics.Point
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import com.unidev.e_steps.R
import com.unidev.e_steps.ui.utils.BLEUtils
import com.unidev.e_steps.ui.utils.PreferencesManager
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*

class RecordActivity : AppCompatActivity() {

    private var ble: BLEUtils? = null
    val cops: MutableList<Point> = mutableListOf()
    val maxPressures: MutableList<Double> = mutableListOf()

    private var buttonText : TextView? = null
    private var pressureChartLeft : ImageView? = null
    private var pressureChartRight : ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_record)

        buttonText = findViewById(R.id.startText)
        pressureChartLeft = findViewById(R.id.pressureGradientLeft)
        pressureChartRight = findViewById(R.id.pressureGradientRight)

        ble = BLEUtils(this)
    }

    override fun onStop() {
        super.onStop()
        stop()
    }

    fun startRecord(view: View) {
        buttonText!!.text = if (buttonText!!.text == resources.getString(R.string.start))
        {
            pressureChartLeft!!.setBackgroundColor(Color.BLACK)
            pressureChartRight!!.setBackgroundColor(Color.BLACK)
            start()
            resources.getString(R.string.stop)
        } else {
            stop()
            resources.getString(R.string.start)
        }
    }

    private fun start() {
        BLEUtils(this).findNearbyDevices(scanCallback)
    }
    private fun stop() {
        ble!!.stop(scanCallback)
        ble!!.disconnectAll()
        val cm = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        Toast.makeText(applicationContext, "internet connected:$isConnected", Toast.LENGTH_SHORT).show()
        if(isConnected){

        }
        else{
            PreferencesManager.savePressures(this,cops,maxPressures)
        }
        finish()
    }

    private val scanCallback = object : ScanCallback() {
        override fun onScanResult(callbackType: Int, result: ScanResult?) {
            super.onScanResult(callbackType, result)
            if (PreferencesManager.getSavedDevices(this@RecordActivity).contains(result?.device?.address))
                result!!.device.connectGatt(this@RecordActivity, true, gattCallback)
        }
    }

    private val gattCallback: BluetoothGattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            if (newState == BluetoothGatt.STATE_CONNECTED) {
                BLEUtils.connectedGatts.add(gatt!!)
                gatt.discoverServices()

                runOnUiThread {
                    if (gatt.device.name.contains("Left"))
                        pressureChartLeft!!.setBackgroundColor(Color.WHITE)
                    if (gatt.device.name.contains("Right"))
                        pressureChartRight!!.setBackgroundColor(Color.WHITE)
                }
            }
        }
        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            super.onServicesDiscovered(gatt, status)
            val firstCharacteristic = gatt!!.services.lastOrNull()?.characteristics?.first()
            gatt.readCharacteristic(firstCharacteristic)
        }


        fun BluetoothDevice.isLeftInsole() = this.name.contains("Left")

        fun BluetoothGattCharacteristic.getColor() : Int {
            val read = this.getDoubleValue()
            return Color.rgb((255 * read).toInt(), 0, (255 * (1-read)).toInt())
        }

        private val maxSize = 300
        private var positionsLeft: MutableList<IntArray> = mutableListOf(
            IntArray(2) { 0 },
            IntArray(2) { 0 },
            IntArray(2) { 0 },
            IntArray(2) { 0 },
            IntArray(2) { 0 },
            IntArray(2) { 0 }
        )

        private var positionsRight: MutableList<IntArray> = mutableListOf(
            IntArray(2) { 0 },
            IntArray(2) { 0 },
            IntArray(2) { 0 },
            IntArray(2) { 0 },
            IntArray(2) { 0 },
            IntArray(2) { 0 }
        )
        private var intensitiesLeft: MutableList<Double> = mutableListOf(0.0,0.0,0.0,0.0,0.0,0.0)
        private var intensitiesRight: MutableList<Double> = mutableListOf(0.0,0.0,0.0,0.0,0.0,0.0)
        override fun onCharacteristicChanged(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?
        ) {
            super.onCharacteristicChanged(gatt, characteristic)
            //println(characteristic!!.toNiceString())

            var value = characteristic!!.getDoubleValue()
            val index = characteristic.uuid.toNiceString()-10
            if (value < 0.02) value = 0.0
            val size = (maxSize * value).toInt()

            lateinit var cop: ImageView
            lateinit var positions: MutableList<IntArray>
            lateinit var intensities: MutableList<Double>
            lateinit var sensorImage: ImageView

                println(characteristic.toNiceString())

            if (gatt!!.device.isLeftInsole()) {
                cop = findViewById(R.id.CoPLeft)
                positions = positionsLeft
                intensities = intensitiesLeft
                sensorImage = when(characteristic.uuid.toNiceString()) {
                    10 -> findViewById(R.id.sensor_left_6)
                    11 -> findViewById(R.id.sensor_left_5)
                    12 -> findViewById(R.id.sensor_left_1)
                    13 -> findViewById(R.id.sensor_left_3)
                    14 -> findViewById(R.id.sensor_left_4)
                    else -> findViewById(R.id.sensor_left_2)
                }
            } else {
                cop = findViewById(R.id.CoPRight)
                positions = positionsRight
                intensities = intensitiesRight
                sensorImage = when(characteristic.uuid.toNiceString()) {
                    10 -> findViewById(R.id.sensor_right_2)
                    11 -> findViewById(R.id.sensor_right_1)
                    12 -> findViewById(R.id.sensor_right_3)
                    13 -> findViewById(R.id.sensor_right_4)
                    14 -> findViewById(R.id.sensor_right_6)
                    else -> findViewById(R.id.sensor_right_5)
                }
            }

            runOnUiThread {
                val layout = sensorImage.layoutParams
                layout.height = size
                layout.width = size
                sensorImage.visibility = if (size == 0) View.GONE else View.VISIBLE
                sensorImage.layoutParams = layout
            }

            var pos = IntArray(2)
            sensorImage.getLocationOnScreen(pos)
            pos[0] += sensorImage.width / 2 - cop.width / 2
            pos[1] += sensorImage.height / 2 - cop.height / 2
            positions[index] = pos
            intensities[index] = value


            val copPos = positions.zip(intensities)
                .map { Pair(it.first[0] * it.second, it.first[1] * it.second) }
                .reduce { p1,p2 -> Pair(p1.first + p2.first, p1.second + p2.second)}
                .toList()
                .map { (it / intensities.sum()).toInt() }

            cops.add(Point(copPos[0],copPos[1]))
            maxPressures.add(intensities.max()!!)

            runOnUiThread {
                val copLayout = cop.layoutParams as RelativeLayout.LayoutParams
                copLayout.leftMargin = copPos[0]
                copLayout.topMargin = copPos[1]
                cop.layoutParams = copLayout
            }
        }

        override fun onCharacteristicRead(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?,
            status: Int
        ) {
            super.onCharacteristicRead(gatt, characteristic, status)
            characteristic!!.subscribe(gatt!!)
        }

        override fun onDescriptorWrite(
            gatt: BluetoothGatt?,
            descriptor: BluetoothGattDescriptor?,
            status: Int
        ) {
            super.onDescriptorWrite(gatt, descriptor, status)
            runOnUiThread {
                Toast.makeText(applicationContext, "Sensor ${descriptor!!.characteristic!!.uuid.toNiceString()} connected.", Toast.LENGTH_SHORT).show()
            }
            readNextCharacteristic(gatt!!, descriptor!!.characteristic)
        }

        private fun BluetoothGattCharacteristic.subscribe(gatt: BluetoothGatt) {
            gatt.setCharacteristicNotification(this, true)
            val descriptor = this.descriptors.first()
            descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            gatt.writeDescriptor(descriptor)
        }

        private fun readNextCharacteristic(gatt: BluetoothGatt, actual: BluetoothGattCharacteristic) {
            val remaining = actual.service.characteristics.dropWhile { it.uuid != actual.uuid }
            if (remaining.count() > 1)
                gatt.readCharacteristic(remaining[1])
        }

        private fun BluetoothGattCharacteristic.getDoubleValue() =
            ByteBuffer.wrap(this.value).order(ByteOrder.LITTLE_ENDIAN).double
        private fun BluetoothGattCharacteristic.toNiceString() = "${this.uuid.toNiceString()} = ${this.getDoubleValue()}"
        private fun UUID.toNiceString() = this.toString().split('-').first().toInt()
    }
}
