package com.unidev.e_steps.ui.fragments.hometabs.diagnostic


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.unidev.e_steps.R

/**
 * A simple [Fragment] subclass.
 */
class SpecificAreaFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_specific_area, container, false)
    }


}
