package com.unidev.e_steps.ui.activities

import android.os.Bundle
import com.unidev.e_steps.R

class PrivacyActivity : NavigationDrawerActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy)

        initToolbar(findViewById(R.id.toolbar), findViewById(
            R.id.drawer_layout
        ))
    }
}
