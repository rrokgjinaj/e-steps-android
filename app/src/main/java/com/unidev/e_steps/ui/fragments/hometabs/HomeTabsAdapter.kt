package com.unidev.e_steps.ui.fragments.hometabs

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.unidev.e_steps.ui.fragments.hometabs.devices.DevicesFragment
import com.unidev.e_steps.ui.fragments.hometabs.diagnostic.DiagnosticFragment
import com.unidev.e_steps.R
import com.unidev.e_steps.ui.fragments.hometabs.home.HomeFragment

private val TAB_TITLES = arrayOf(
    R.string.tab_text_1,
    R.string.diagnostic,
    R.string.tab_text_2
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class HomeTabsAdapter(private val context: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when(position)
        {
            0-> HomeFragment()
            1-> DiagnosticFragment()
            else-> DevicesFragment()
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        return TAB_TITLES.count()
    }
}