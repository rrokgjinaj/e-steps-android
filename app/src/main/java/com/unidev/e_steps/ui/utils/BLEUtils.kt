package com.unidev.e_steps.ui.utils

import android.Manifest
import android.app.Activity
import android.bluetooth.*
import android.bluetooth.le.ScanCallback
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.os.Build
import android.provider.Settings
import androidx.core.content.PermissionChecker
import androidx.fragment.app.Fragment

class BLEUtils (private val activity: Activity) {
    private var fragment: Fragment? = null

    constructor(fragment: Fragment) : this(fragment.activity!!) {
        this.fragment = fragment
    }

    private enum class PermissionCodes { LOCATION, BLUETOOTH, GPS }

    private val bluetoothManager = activity.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
    private val bluetoothAdapter = bluetoothManager.adapter
    private val scanner = bluetoothAdapter!!.bluetoothLeScanner

    private fun askNeededThings() {
        if (!bluetoothAdapter!!.isEnabled)
            activity.startActivityForResult(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), PermissionCodes.BLUETOOTH.ordinal)

        val locationManager = activity.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            activity.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))

        if (!hasPermissions())
            requestPermissions()
    }

    fun findNearbyDevices(callback: ScanCallback) {
        askNeededThings()
        if (hasPermissions())
            startScan(callback)
    }
    fun stop(callback: ScanCallback) = scanner.stopScan(callback)

    private fun hasPermissions() : Boolean {
        return (PermissionChecker.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PermissionChecker.PERMISSION_GRANTED) &&
                (PermissionChecker.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PermissionChecker.PERMISSION_GRANTED) &&
                (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q ||
                        PermissionChecker.checkSelfPermission(activity, Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                        == PermissionChecker.PERMISSION_GRANTED)
    }
    private fun requestPermissions() {
        if (!hasPermissions()) {
            val permissions =
                mutableListOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                permissions.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
            if (fragment != null)
                fragment!!.requestPermissions(permissions.toTypedArray(), PermissionCodes.LOCATION.ordinal)
            else
                activity.requestPermissions(permissions.toTypedArray(), PermissionCodes.LOCATION.ordinal)
        }
    }

    private fun startScan(callback: ScanCallback) {
        //Toast.makeText(activity, "Scan start", Toast.LENGTH_LONG).show()
        scanner!!.startScan(callback)
    }

    fun disconnectAll() {
        connectedGatts.forEach { it.disconnect() }
        connectedGatts.clear()
    }

    companion object {
        var connectedGatts: MutableSet<BluetoothGatt> = mutableSetOf()
    }
}