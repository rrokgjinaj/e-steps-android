package com.unidev.e_steps.ui.activities

import android.bluetooth.BluetoothDevice
import android.os.Bundle
import android.content.Intent
import android.view.View
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import com.unidev.e_steps.R
import com.unidev.e_steps.ui.fragments.scan.ScanDevicesFragment
import com.unidev.e_steps.ui.utils.PreferencesManager
import kotlinx.android.synthetic.main.activity_configuration_scan.*


class ScanSettingsActivity : AppCompatActivity(), ScanDevicesFragment.Handlers {

    override fun onConnection(device: BluetoothDevice) {
        //val button = findViewById<Button>(R.id.goHome)
        goHome.isEnabled = true
        goHome.alpha = 1F
        goHome.text = getString(R.string.done)

        PreferencesManager.addDevice(this, device)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.requestWindowFeature(Window.FEATURE_ACTION_BAR)
        setContentView(R.layout.activity_configuration_scan)
    }

    fun goHome(view: View) {
        //scanner!!.stopScan(scanCallback)
        startActivity(Intent(this, HomeActivity::class.java))
    }
}
