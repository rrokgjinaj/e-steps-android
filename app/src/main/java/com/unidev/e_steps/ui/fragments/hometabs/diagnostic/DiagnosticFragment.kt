package com.unidev.e_steps.ui.fragments.hometabs.diagnostic
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.unidev.e_steps.R


class DiagnosticFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_diagnostic, container, false)

        root.findViewById<Button>(R.id.specific_area_button).setOnClickListener {
            fragmentManager!!.beginTransaction().replace(
                R.id.fragment_diagnostic,
                SpecificAreaFragment()
            ).commit()
        }
        root.findViewById<Button>(R.id.full_body_button).setOnClickListener {
            fragmentManager!!.beginTransaction().replace(
                R.id.fragment_diagnostic,
                FullBodyFragment()
            ).commit()
        }


        return root
    }
}
