package com.unidev.e_steps.ui.activities

import android.content.Intent
import android.view.MenuItem

import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import com.unidev.e_steps.R
import com.unidev.e_steps.ui.utils.PreferencesManager
import kotlin.system.exitProcess

abstract class NavigationDrawerActivity : AppCompatActivity() {
    fun initToolbar(toolbar: Toolbar, drawerLayout: DrawerLayout) {
        setSupportActionBar(toolbar)
        val bar = ActionBarDrawerToggle(this, drawerLayout, toolbar,
            R.string.start,
            R.string.start
        )
        bar.isDrawerIndicatorEnabled = true
        bar.syncState()
    }

    fun goProfile(item: MenuItem) {
        if (this !is ProfileActivity)
            startActivity( Intent(this, ProfileActivity::class.java))
    }
    fun goDevices(item: MenuItem) {
        if (this !is DevicesActivity)
            startActivity( Intent(this, DevicesActivity::class.java))
    }
    fun goHome(item: MenuItem) {
        if (this !is HomeActivity)
            startActivity( Intent(this, HomeActivity::class.java))
    }

    fun closeApplication(item: MenuItem) {
        PreferencesManager.removeUser(this)
        startActivity(Intent(this, LoginActivity::class.java).apply {
            this.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            this.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            this.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            //this.putExtra("")
        })
        //finish()
        //exitProcess(0)
    }

    fun goAbout(item: MenuItem) {
        if (this !is AboutActivity)
            startActivity( Intent(this, AboutActivity::class.java))
    }
    fun goPrivacy(item: MenuItem) {
        if (this !is PrivacyActivity)
            startActivity(Intent(this, PrivacyActivity::class.java))
    }
}
