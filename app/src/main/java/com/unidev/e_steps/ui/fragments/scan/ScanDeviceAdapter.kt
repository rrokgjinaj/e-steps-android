package com.unidev.e_steps.ui.fragments.scan

import android.app.Activity
import android.bluetooth.*
import android.content.Context
import android.content.ContextWrapper
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.unidev.e_steps.ui.fragments.AbstractDeviceAdapter
import com.unidev.e_steps.R
import com.unidev.e_steps.ui.utils.BLEUtils
import com.unidev.e_steps.ui.utils.PreferencesManager

class ScanDeviceAdapter(val context: Context, private val data: Set<BluetoothDevice>) :
    AbstractDeviceAdapter<BluetoothDevice, ScanDeviceAdapter.ViewHolder>(context, data,
        R.layout.device_row, { v ->
            ViewHolder(v)
        }) {

    /*private val activity =
        if (context is Activity)
            context
        else
            (context as ContextWrapper).baseContext as Activity*/

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        holder.textView.text = data.elementAt(position).name
        holder.button.setOnClickListener { holder.connectButtonHandler(data.elementAt(position))}
        //holder.listener = activity as ScanDevicesFragment.Handlers
    }

    class ViewHolder(itemView: View) : AbstractViewHolder<BluetoothDevice>(itemView) {
        val textView: TextView = itemView.findViewById(R.id.textView)
        val button: Button = itemView.findViewById(R.id.connectButton)
        //var listener: ScanDevicesFragment.Handlers? = null

        private val activity =
            if (itemView.context is Activity)
                itemView.context as Activity
            else
                (itemView.context as ContextWrapper).baseContext as Activity

        fun connectButtonHandler(device: BluetoothDevice) {
            device.connectGatt( itemView.context, true, gattCallback)
        }

        override fun onItemClick(device: BluetoothDevice) {
            if (button.isEnabled)
                button.callOnClick()
        }

        fun onConnectionSuccess(device: BluetoothDevice)
        {
            button.isEnabled= false
            button.alpha= 0.3F
            button.text = itemView.context.getString(R.string.connected)
            (activity as ScanDevicesFragment.Handlers).onConnection(device)
        }

        private val gattCallback: BluetoothGattCallback = object : BluetoothGattCallback() {
            override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
                super.onConnectionStateChange(gatt, status, newState)
                if (newState == BluetoothGatt.STATE_CONNECTED) {
                    PreferencesManager.addDevice(activity, gatt!!.device)
                    BLEUtils.connectedGatts.add(gatt)

                    activity.runOnUiThread {
                        onConnectionSuccess(gatt.device)
                    }
                }
            }
        }
    }
}