package com.unidev.e_steps.ui.activities
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.unidev.e_steps.R
import java.sql.Connection
import java.text.SimpleDateFormat
import java.util.*


class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
    }

    fun registerUser(view: View) {

        val properties = Properties()
        //Populate the properties file with user name and password
        with(properties) {
            put("user", "yeRpXCtWU0")
            put("password", "YNS8mm7jwO")
        }


        //Open a connection to the database
       /* Class.forName("com.mysql.jdbc.Driver").newInstance()
        DriverManager
            .getConnection("jdbc:mysql://remotemysql.com:3306/YNS8mm7jwO", properties)
            .use { connection ->
                insertRow(connection,"yeRpXCtWU0","estepper",
                    findViewById<TextView>(R.id.email).text.toString(),
                    findViewById<TextView>(R.id.password).text.toString()

                    /*,findViewById<TextView>(R.id.birthdate).text.toString(),
                    findViewById<TextView>(R.id.weight).text.toString(),
                    findViewById<TextView>(R.id.height).text.toString(),
                    findViewById<Spinner>(R.id.gender).selectedItem.toString(),
                    findViewById<TextView>(R.id.shoe).text.toString(),
                    findViewById<TextView>(R.id.name).text.toString(),
                    findViewById<TextView>(R.id.lastname).text.toString()*/
                )
                print("------------------ insert")
            }*/
        startActivity(Intent(this, LoginActivity::class.java))
    }
    //email: String, password:String,name:String,lastname:String,birthdate:Date,weight:
    fun insertRow(connection: Connection, schema : String, table : String, vararg params:String) {
        val sql = "INSERT INTO $schema.$table VALUES (${params.joinToString(",")})"
        with(connection) {
            createStatement().execute(sql)
            commit()
        }
    }

    fun openDatePicker(birthdate: View) {
        val myCalendar = Calendar.getInstance()
        val date = OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                myCalendar[Calendar.YEAR] = year
                myCalendar[Calendar.MONTH] = monthOfYear
                myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
                this.updateLabel(birthdate as EditText,myCalendar)
            }
            DatePickerDialog(
                this@RegisterActivity, date, myCalendar[Calendar.YEAR], myCalendar[Calendar.MONTH],
                myCalendar[Calendar.DAY_OF_MONTH]
            ).show()

    }

    private fun updateLabel(edittext:EditText,myCalendar:Calendar) {
        val myFormat = "dd/MM/yy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        edittext.setText(sdf.format(myCalendar.getTime()))
    }
}
