package com.unidev.e_steps.data

data class CustomDevice(val address:String, val name: String, var state:CustomBTState)
enum class CustomBTState { Online, Unavailable }